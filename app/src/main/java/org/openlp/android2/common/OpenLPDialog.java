/******************************************************************************
 * OpenLP - Open Source Lyrics Projection                                      *
 * --------------------------------------------------------------------------- *
 * Copyright (c) 2011-2016 OpenLP Android Developers                           *
 * --------------------------------------------------------------------------- *
 * This program is free software; you can redistribute it and/or modify it     *
 * under the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; version 2 of the License.                              *
 * *
 * This program is distributed in the hope that it will be useful, but WITHOUT *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    *
 * more details.                                                               *
 * *
 * You should have received a copy of the GNU General Public License along     *
 * with this program; if not, write to the Free Software Foundation, Inc., 59  *
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA                          *
 *******************************************************************************/
package org.openlp.android2.common;

import android.app.DialogFragment;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.openlp.android2.R;
import org.openlp.android2.api.Api;

import java.util.HashMap;
import java.util.Map;


abstract public class OpenLPDialog extends DialogFragment {

    private final String LOG_TAG = OpenLPDialog.class.getName();

    protected Context context;
    private String urlcalled;

    protected void populateDisplay(String responseString) {
    }

    protected void processUpdate(String responseString) {
    }

    protected void errorDisplay(String responseString) {
    }

    protected void triggerTextRequest(final String urlbase) {
        String url = RequestQueueService.getInstance(this.context).getUrl(urlbase);
        urlcalled = urlbase;

        StringRequest request = new StringRequest(
                Request.Method.GET,
                url,
                listener,
                errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createBasicAuthHeader("user", "passwd");
            }
        };
        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        // Volley does retry for you if you have specified the policy.
        request.setRetryPolicy(new DefaultRetryPolicy(
                RequestQueueService.getInstance(this.context).getConnectionTimeout(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag("OpenLP");
        RequestQueueService.getInstance(this.context).addToRequestQueue(request);
    }

    Map<String, String> createBasicAuthHeader(String username, String password) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", RequestQueueService.getInstance(context).getBasicAuth());

        return headers;
    }

    Response.Listener<String> listener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            if (urlcalled.equals(Api.POLL_STATUS)) {
                populateDisplay(response);
            } else {
                processUpdate(response);
            }
        }
    };

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(LOG_TAG, String.format("Call response error = %s", error.toString()));
            if (error instanceof NetworkError) {
            } else if (error instanceof ClientError) {
            } else if (error instanceof ServerError) {
            } else if (error instanceof AuthFailureError) {
                Toast.makeText(context, R.string.httpreturn_unauthorised,
                        Toast.LENGTH_LONG).show();
            } else if (error instanceof ParseError) {
            } else if (error instanceof NoConnectionError) {
            } else if (error instanceof TimeoutError) {
            }
            Toast.makeText(context, R.string.unable,
                    Toast.LENGTH_LONG).show();
            errorDisplay(error.toString());

        }
    };
}
