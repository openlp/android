/******************************************************************************
 * OpenLP - Open Source Lyrics Projection                                      *
 * --------------------------------------------------------------------------- *
 * Copyright (c) 2011-2016 OpenLP Android Developers                           *
 * --------------------------------------------------------------------------- *
 * This program is free software; you can redistribute it and/or modify it     *
 * under the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; version 2 of the License.                              *
 *                                                                             *
 * This program is distributed in the hope that it will be useful, but WITHOUT *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    *
 * more details.                                                               *
 *                                                                             *
 * You should have received a copy of the GNU General Public License along     *
 * with this program; if not, write to the Free Software Foundation, Inc., 59  *
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA                          *
 *******************************************************************************/
package org.openlp.android2.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import org.openlp.android2.R;

public class OpenLPURLBuilder {
    private final String LOG_TAG = OpenLPURLBuilder.class.getName();
    private Context context;

    private static OpenLPURLBuilder ourInstance = new OpenLPURLBuilder();

    public static OpenLPURLBuilder getInstance() {
        return ourInstance;
    }

    private OpenLPURLBuilder() {
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getConnectionTimeout() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        int connectionTimeout = context.getResources().getInteger(
                R.integer.connectionTimeoutDefaultValue);

        if (sharedPrefs.getBoolean(context.getString(R.string.key_enable_custom_timeout), false)) {
            Log.d(LOG_TAG, "Overriding Connection and Socket timeouts");

            connectionTimeout = Integer.parseInt(sharedPrefs.getString(context.getString(R.string.key_connection_timeout),
                    String.valueOf(context.getResources().getInteger(R.integer.connectionTimeoutDefaultValue))
            ));
        }
        return connectionTimeout;
    }

    public String getBasicAuth(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String userid = sharedPrefs.getString(context.getString(R.string.key_userid), "openlp");
        String password = sharedPrefs.getString(context.getString(R.string.key_password), "password");

        Log.d(LOG_TAG, "Credentials set to " + userid + " : " + password);

        String credentials = userid + ":" + password;
        return "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
    }

    public String getBaseUrl(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        String host = sharedPrefs.getString(context.getString(R.string.key_host),
                context.getString(R.string.host_default_value));
        String port = sharedPrefs.getString(context.getString(R.string.key_port), "4316");

        return String.format("http://%s:%s", host, port);

    }
}
