package org.openlp.android2.common;

import android.content.Context;

/**
 * Created by tim on 14/08/16.
 */
public class StateHolder {
    private static StateHolder ourInstance = new StateHolder();
    private static Context context;

    public static StateHolder getInstance() {
        return ourInstance;
    }

    private StateHolder() {
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
